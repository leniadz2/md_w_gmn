﻿CREATE TABLE [stg].[SALDO_MC] (
  [CODIGOSEGUIMIENTO] [varchar](50) NULL,
  [INSTITUCION] [varchar](50) NULL,
  [EMPRESA] [varchar](50) NULL,
  [CLIENTE] [varchar](50) NULL,
  [NROTARJETA] [varchar](50) NULL,
  [FECPROCESO] [varchar](50) NULL,
  [MONEDA] [varchar](50) NULL,
  [SALDOARCHIVOUBA] [float] NULL,
  [SALDOSIMP] [float] NULL,
  [ESTADO] [varchar](50) NULL,
  [IDCUENTA] [varchar](50) NULL,
  [BIN] [varchar](50) NULL,
  [SUBBIN] [varchar](50) NULL,
  [MES] [varchar](7) NOT NULL,
  [DIA] [varchar](10) NOT NULL
)
ON [PRIMARY]
GO