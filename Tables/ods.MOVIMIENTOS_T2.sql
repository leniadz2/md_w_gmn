﻿CREATE TABLE [ods].[MOVIMIENTOS_T2] (
  [TRANSACTION_ID] [varchar](50) NULL,
  [CELULAR] [varchar](9) NULL,
  [DATE] [varchar](8) NOT NULL,
  [AMOUNT] [float] NULL,
  [COMMISSION] [float] NULL,
  [ACTOR] [varchar](11) NOT NULL,
  [TIPO] [varchar](15) NOT NULL,
  [AMBITO] [varchar](7) NOT NULL,
  [PERIODO] [varchar](6) NOT NULL,
  [COD_CANAL] [varchar](50) NULL,
  [TRANSACTION_TYPE] [varchar](50) NULL,
  [KasNet_CashOut] [float] NULL,
  [KasNet_CashIn] [float] NULL,
  [BCP_CashIn] [float] NULL,
  [Prymera_CashOut] [float] NULL,
  [Prymera_CashIn] [float] NULL,
  [A_tarjeta_Envio] [float] NULL,
  [A_tarjeta_Recupero] [float] NULL,
  [SCB_CashIn] [float] NULL,
  [BE_Transferencia] [float] NULL,
  [BE_PgoServicios] [float] NULL,
  [BE_RecargaCelular] [float] NULL,
  [BE_AJUSTES] [float] NULL,
  [ORDDATEDESC] [bigint] NULL
)
ON [PRIMARY]
GO

CREATE INDEX [MOVIMIENTOS_T2_I1]
  ON [ods].[MOVIMIENTOS_T2] ([CELULAR])
  ON [PRIMARY]
GO