﻿CREATE TABLE [stg].[TARJETA] (
  [ID_TARJETA] [varchar](50) NULL,
  [NRO_TARJETA] [varchar](50) NULL,
  [CODIGOSEGUIMIENTO] [varchar](50) NULL,
  [FECHA_ASOCIACION_TARJ] [varchar](50) NULL,
  [ESTADO_TARJETA] [varchar](50) NULL,
  [FECHA_ACTIVACION] [varchar](50) NULL,
  [TIPO_BLOQUEO] [varchar](50) NULL,
  [FECHA_BLOQUEO] [varchar](50) NULL,
  [POSEE_DUENO] [varchar](50) NULL,
  [ID_PERSONA] [varchar](50) NULL,
  [CODIGO_UBA] [varchar](50) NULL,
  [TIPODOCUMENTO] [varchar](4) NULL,
  [DNI] [varchar](8) NULL,
  [NOMBRES] [varchar](50) NULL,
  [APELLIDO_PATERNO] [varchar](50) NULL,
  [APELLIDO_MATERNO] [varchar](50) NULL,
  [NOMBRE] [varchar](152) NOT NULL,
  [CELULAR] [varchar](9) NULL,
  [DNI_CELULAR] [varchar](17) NOT NULL,
  [REGIMEN] [varchar](50) NULL,
  [CATEGORIA_REGIMEN] [varchar](50) NULL,
  [FECHA_ORDEN] [varchar](50) NULL,
  [BIN] [varchar](50) NULL,
  [SUBBIN] [varchar](50) NULL,
  [AFINIDAD] [varchar](50) NULL,
  [TIPO_TARJETA] [varchar](50) NULL,
  [FISICA_NACIDA_VIRTUAL] [varchar](50) NULL,
  [FEC_VIRT_A_FIS] [varchar](50) NULL,
  [HABILIACION_VIRT_A_FIS] [varchar](50) NULL,
  [FEC_HABILIACION_VIRT_A_FIS] [varchar](50) NULL,
  [CLIENTE] [varchar](50) NULL,
  [EMPRESA] [varchar](50) NULL,
  [INSTITUCION] [varchar](50) NULL,
  [CUENTA] [varchar](50) NULL,
  [MONEDA_CUENTA] [varchar](50) NULL,
  [ID_LOTE_STOCK] [varchar](50) NULL,
  [TRACE_LOTE_STOCK] [varchar](50) NULL
)
ON [PRIMARY]
GO