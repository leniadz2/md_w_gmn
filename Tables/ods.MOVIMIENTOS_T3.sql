﻿CREATE TABLE [ods].[MOVIMIENTOS_T3] (
  [CELULAR] [varchar](9) NULL,
  [COMMISSION] [float] NULL,
  [KasNet_CashOut] [float] NULL,
  [KasNet_CashIn] [float] NULL,
  [BCP_CashIn] [float] NULL,
  [Prymera_CashOut] [float] NULL,
  [Prymera_CashIn] [float] NULL,
  [A_tarjeta_Envio] [float] NULL,
  [A_tarjeta_Recupero] [float] NULL,
  [SCB_CashIn] [float] NULL,
  [BE_Transferencia] [float] NULL,
  [BE_PgoServicios] [float] NULL,
  [BE_RecargaCelular] [float] NULL,
  [BE_AJUSTES] [float] NULL,
  [count_KasNet_CashOut] [int] NULL,
  [count_KasNet_CashIn] [int] NULL,
  [count_BCP_CashIn] [int] NULL,
  [count_Prymera_CashOut] [int] NULL,
  [count_Prymera_CashIn] [int] NULL,
  [count_A_tarjeta_Envio] [int] NULL,
  [count_A_tarjeta_Recupero] [int] NULL,
  [count_SCB_CashIn] [int] NULL,
  [count_BE_Transferencia] [int] NULL,
  [count_BE_PgoServicios] [int] NULL,
  [count_BE_RecargaCelular] [int] NULL,
  [count_BE_AJUSTES] [int] NULL,
  [FECULT_CI] [varchar](8) NULL
)
ON [PRIMARY]
GO

CREATE INDEX [MOVIMIENTOS_T3_I1]
  ON [ods].[MOVIMIENTOS_T3] ([CELULAR])
  ON [PRIMARY]
GO