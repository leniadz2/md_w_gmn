﻿CREATE TABLE [ods].[MOVIMIENTOS_T1] (
  [TRANSACTION_ID] [varchar](50) NULL,
  [CELULAR] [varchar](9) NULL,
  [DATE] [varchar](8) NOT NULL,
  [AMOUNT] [float] NULL,
  [COMMISSION] [float] NULL,
  [ACTOR] [varchar](11) NOT NULL,
  [TRANSACTION_TYPE] [varchar](50) NULL,
  [TIPO] [varchar](15) NOT NULL,
  [AMBITO] [varchar](7) NOT NULL,
  [PERIODO] [varchar](6) NOT NULL,
  [COD_CANAL] [varchar](50) NULL
)
ON [PRIMARY]
GO