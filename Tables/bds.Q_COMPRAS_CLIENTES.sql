﻿CREATE TABLE [bds].[Q_COMPRAS_CLIENTES] (
  [Tipo mensaje] [varchar](50) NULL,
  [Hora local Compra] [varchar](50) NULL,
  [Fecha local Compra] [varchar](50) NULL,
  [REFERENCE_NUMBER] [varchar](50) NULL,
  [AUTHORIZATION_CODE] [varchar](50) NULL,
  [RESPONSE_CODE] [varchar](50) NULL,
  [CARD_ACCEPTOR_TERMID] [varchar](50) NULL,
  [CARD_ACCEPTOR_LOCATION] [varchar](100) NULL,
  [ID_CODIGO_TRANSACCION] [varchar](50) NULL,
  [ID_CLASE_TRANSACCION] [varchar](50) NULL,
  [ID_CLIENTE] [varchar](50) NULL,
  [ID_CANAL] [varchar](50) NULL,
  [INSTITUCION_RECEPTORA] [varchar](50) NULL,
  [CODIGOSEGUIMIENTO] [varchar](50) NULL,
  [TARJETA_PRESENTE] [varchar](50) NULL,
  [Día Compra] [varchar](2) NULL,
  [Periodo Compra] [varchar](6) NOT NULL,
  [SELLER] [varchar](100) NULL,
  [Tipo] [varchar](10) NOT NULL,
  [Monto compra] [float] NULL,
  [Compras contador] [int] NOT NULL,
  [Resultado compra] [varchar](16) NULL,
  [Nombre] [varchar](152) NOT NULL,
  [DNI_CELULAR] [varchar](17) NULL,
  [Tipo documento] [varchar](4) NULL,
  [DNI] [varchar](8) NULL,
  [Celular] [varchar](9) NULL,
  [Primer nombre] [varchar](50) NULL,
  [Segundo nombre] [varchar](50) NULL,
  [Tercer nombre] [varchar](50) NULL,
  [Cuarto nombre] [varchar](50) NULL,
  [Quinto nombre] [varchar](50) NULL,
  [Primer apellido] [varchar](50) NULL,
  [Segundo apellido] [varchar](50) NULL,
  [Correo electrónico] [varchar](50) NULL,
  [AFFILIATE_CODE] [varchar](50) NULL,
  [Status] [varchar](50) NULL,
  [Habilitado recarga] [varchar](50) NULL,
  [Segmento] [varchar](50) NULL,
  [Fecha nacimiento] [varchar](8) NULL,
  [Edad] [int] NULL,
  [Fec. Cre. Cli.] [varchar](8) NULL,
  [Hora Cre. Cli.] [varchar](8) NULL,
  [Hora HH Cre. Cli.] [varchar](2) NULL,
  [Afiliador] [varchar](2) NULL,
  [Afiliado] [varchar](12) NULL,
  [Genero] [varchar](11) NULL,
  [Periodo Cre. Cli.] [varchar](6) NULL,
  [B.E. Saldo] [float] NULL,
  [MC Saldo] [float] NULL
)
ON [PRIMARY]
GO