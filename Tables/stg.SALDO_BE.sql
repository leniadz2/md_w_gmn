﻿CREATE TABLE [stg].[SALDO_BE] (
  [DNI] [varchar](8) NULL,
  [FEC_ACTUALIZACION] [varchar](14) NOT NULL,
  [TIPODOCUMENTO] [varchar](4) NULL,
  [CELULAR] [varchar](9) NULL,
  [DNI_CELULAR] [varchar](17) NOT NULL,
  [FECNACIMIENTO] [varchar](8) NOT NULL,
  [EDAD] [int] NULL,
  [SALDO] [float] NULL
)
ON [PRIMARY]
GO