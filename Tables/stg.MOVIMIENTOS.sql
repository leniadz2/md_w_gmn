﻿CREATE TABLE [stg].[MOVIMIENTOS] (
  [TRANSACTION_ID] [varchar](50) NULL,
  [CELULAR] [varchar](9) NULL,
  [DATE] [varchar](8) NOT NULL,
  [AMOUNT] [float] NULL,
  [COMMISSION] [float] NULL,
  [ACTOR] [varchar](11) NOT NULL,
  [TRANSACTION_TYPE] [varchar](50) NULL,
  [TIPO] [varchar](15) NOT NULL,
  [AMBITO] [varchar](7) NOT NULL,
  [PERIODO] [varchar](6) NOT NULL
)
ON [PRIMARY]
GO