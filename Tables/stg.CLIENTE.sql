﻿CREATE TABLE [stg].[CLIENTE] (
  [DNI_CELULAR] [varchar](17) NOT NULL,
  [TIPODOCUMENTO] [varchar](4) NULL,
  [DNI] [varchar](8) NULL,
  [CELULAR] [varchar](9) NULL,
  [FIRST_NAME] [varchar](50) NULL,
  [SECOND_NAME] [varchar](50) NULL,
  [THIRD_NAME] [varchar](50) NULL,
  [FOURTH_NAME] [varchar](50) NULL,
  [FIFTH_NAME] [varchar](50) NULL,
  [FIRST_SURNAME] [varchar](50) NULL,
  [SECOND_SURNAME] [varchar](50) NULL,
  [EMAIL] [varchar](50) NULL,
  [AFFILIATE_CODE] [varchar](50) NULL,
  [STATUS] [varchar](50) NULL,
  [HABILITADO_RECARGA] [varchar](50) NULL,
  [SEGMENTO] [varchar](50) NULL,
  [FECNACIMIENTO] [varchar](8) NOT NULL,
  [EDAD] [int] NULL,
  [FECCREACION] [varchar](8) NOT NULL,
  [HORACREACION] [varchar](8) NULL,
  [HORAHHCREACION] [varchar](2) NULL,
  [AFILIADOR] [varchar](2) NOT NULL,
  [AFILIADO] [varchar](12) NOT NULL,
  [GENERO] [varchar](11) NOT NULL,
  [PER_CREACION_CLIENTE] [varchar](6) NOT NULL
)
ON [PRIMARY]
GO