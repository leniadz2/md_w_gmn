﻿SET QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
CREATE PROCEDURE [ods].[GM_CARGA]
AS
  /***************************************************************************************************
  Procedure:          ods.GM_CARGA
  Create Date:        20211100
  Author:             dÁlvarez
  Description:        carga el proceso de tablas GMONEY
  Call by:            tbd
  Affected table(s):  varias
  Used By:            BI
  Parameter(s):       none
  Log:                none
  Prerequisites:      tbd
  ****************************************************************************************************
  SUMMARY OF CHANGES
  Date(YYYYMMDD)      Author              Comments
  ------------------- ------------------- ------------------------------------------------------------
  20211100            dÁlvarez            ajustes nuevo archivo acumulado
  
  ***************************************************************************************************/


--stg_________________________________________________________________________________________________

--RECHAZOS_LOGIN
--MATI
TRUNCATE TABLE stg.RECHAZOS_LOGIN;

INSERT INTO stg.RECHAZOS_LOGIN
SELECT IDENTITY_ID
      ,VERIFICATION_DATE
      ,STATUS
      ,FLOW_NAME
      ,LIVENESS
      ,CASE
         WHEN DOCUMENT_TYPE IS NULL THEN NULL
         WHEN DOCUMENT_TYPE = 'national-id' THEN 'DNI'
         ELSE 'OTRO'
       END AS TIPODOCUMENTO
      ,IIF(DOCUMENT_TYPE = 'national-id',  RIGHT(CONCAT('00000000',TRIM(DOCUMENT_NUMBER)),8),NULL) AS DNI
      ,COUNTRY
      ,LIVENESS_VIDEO_LINK
      ,SELFIE_LINK
      ,FRONT_IMAGE_LINK
      ,BACK_IMAGE_LINK
      ,FULL_NAME
      ,DATE_OF_BIRTH
      ,EXPIRATION_DATE
      ,SELFIE
      ,FACEMATCH
      ,FACEMATCH_SCORE
      ,DOC_ALTERATION
      ,TEMPLATE_MATCHING
      ,WATCH_LISTS
      ,METADATA
      ,PHONE_NUMBER
      ,PHONE_OWNERSHIP_VALIDATION
      ,EMAIL_ADDRESS
      ,EMAIL_OWNERSHIP_VALIDATION
      ,EMAIL_RISK_VALIDATION
      ,EMAIL_RISK_SCORE
      ,IIF(FACEMATCH ='Passed','SI','NO') AS ROSTRO
      ,IIF(DOC_ALTERATION ='Passed','SI','NO') AS DOCUMENTO
      ,IIF(TEMPLATE_MATCHING ='Passed','SI','NO') AS TEMPLATE
      ,IIF(WATCH_LISTS ='Passed','SI','NO') AS LISTAS --Watch Lists
      ,IIF(FACEMATCH ='Passed' AND DOC_ALTERATION ='Passed' AND TEMPLATE_MATCHING ='Passed' AND WATCH_LISTS ='Passed','NO','SI') AS SELF
  FROM stg.ZGM_MATI;


--TARJETA
--GM_TARJ
TRUNCATE TABLE stg.TARJETA;

INSERT INTO stg.TARJETA
SELECT gmt.ID_TARJETA
      ,gmt.NRO_TARJETA
      ,gmt.CODIGO_DE_SEGUIMIENTO AS CODIGOSEGUIMIENTO
      ,gmt.FECHA_ASOCIACION_TARJ
      ,gmt.ESTADO_TARJETA
      ,gmt.FECHA_ACTIVACION
      ,gmt.TIPO_BLOQUEO
      ,gmt.FECHA_BLOQUEO
      ,gmt.POSEE_DUENO
      ,gmt.ID_PERSONA
      ,gmt.CODIGO_UBA
      ,CASE
         WHEN gmt.TIPO_DOCUMENTO IS NULL THEN NULL
         WHEN gmt.TIPO_DOCUMENTO = '1 - DNI' THEN 'DNI'
         ELSE 'OTRO'
       END AS TIPODOCUMENTO
      ,IIF(gmt.TIPO_DOCUMENTO = '1 - DNI',  RIGHT(CONCAT('00000000',TRIM(gmt.NRO_DOCUMENTO)),8),NULL) AS DNI
      ,gmt.NOMBRES
      ,gmt.APELLIDO_PATERNO
      ,gmt.APELLIDO_MATERNO
      ,CONCAT(gmt.NOMBRES, ' ', gmt.APELLIDO_PATERNO, ' ', gmt.APELLIDO_MATERNO) AS NOMBRE
      ,RIGHT(CONCAT('000000000',gmt.CELULAR),9) AS CELULAR
      ,CONCAT(IIF(gmt.TIPO_DOCUMENTO = '1 - DNI',  RIGHT(CONCAT('00000000',TRIM(gmt.NRO_DOCUMENTO)),8),NULL),RIGHT(CONCAT('000000000',gmt.CELULAR),9)) AS DNI_CELULAR
      ,gmt.REGIMEN
      ,gmt.CATEGORIA_REGIMEN
      ,gmt.FECHA_ORDEN
      ,gmt.BIN
      ,gmt.SUBBIN
      ,gmt.AFINIDAD
      ,gmt.TIPO_TARJETA
      ,gmt.FISICA_NACIDA_VIRTUAL
      ,gmt.FEC_VIRT_A_FIS
      ,gmt.HABILIACION_VIRT_A_FIS
      ,gmt.FEC_HABILIACION_VIRT_A_FIS
      ,gmt.CLIENTE
      ,gmt.EMPRESA
      ,gmt.INSTITUCION
      ,gmt.CUENTA
      ,gmt.MONEDA_CUENTA
      ,gmt.ID_LOTE_STOCK
      ,gmt.TRACE_LOTE_STOCK
 FROM stg.ZGM_TARJ gmt;


--SALDO_BE
--CONC_SLD
TRUNCATE TABLE stg.SALDO_BE;

INSERT INTO stg.SALDO_BE
SELECT IIF(TIPO_DE_DOCUMENTO = '2',  RIGHT(CONCAT('00000000',TRIM(NÚMERO_DE_DOCUMENTO)),8),NULL) AS DNI
      ,CONCAT(SUBSTRING(FECHA_ULTIMA_ACT,7,4),
              SUBSTRING(FECHA_ULTIMA_ACT,1,2),
              SUBSTRING(FECHA_ULTIMA_ACT,4,2),
              SUBSTRING(HORA_ULTIMA_ACT,1,2),
              SUBSTRING(HORA_ULTIMA_ACT,4,2),
              SUBSTRING(HORA_ULTIMA_ACT,7,2)) AS FEC_ACTUALIZACION
      ,CASE
         WHEN TIPO_DE_DOCUMENTO IS NULL THEN NULL
         WHEN TIPO_DE_DOCUMENTO = '02' THEN 'DNI'
         ELSE 'OTRO'
       END AS TIPODOCUMENTO
      ,RIGHT(CONCAT('000000000',CELULAR),9) AS CELULAR
      ,CONCAT(IIF(TIPO_DE_DOCUMENTO = '2',  RIGHT(CONCAT('00000000',TRIM(NÚMERO_DE_DOCUMENTO)),8),NULL),RIGHT(CONCAT('000000000',CELULAR),9)) AS DNI_CELULAR
      ,CONCAT(SUBSTRING(FECHA_DE_NACIMIENTO,7,4),SUBSTRING(FECHA_DE_NACIMIENTO,1,2),SUBSTRING(FECHA_DE_NACIMIENTO,4,2)) AS FECNACIMIENTO
      ,DATEDIFF(YY, CONVERT(DATETIME, CONCAT(SUBSTRING(FECHA_DE_NACIMIENTO,7,4),SUBSTRING(FECHA_DE_NACIMIENTO,1,2),SUBSTRING(FECHA_DE_NACIMIENTO,4,2))), GETDATE()) -
       CASE 
           WHEN DATEADD(YY,DATEDIFF(YY, CONVERT(DATETIME, CONCAT(SUBSTRING(FECHA_DE_NACIMIENTO,7,4),SUBSTRING(FECHA_DE_NACIMIENTO,1,2),SUBSTRING(FECHA_DE_NACIMIENTO,4,2))), GETDATE()),CONVERT(DATETIME, CONCAT(SUBSTRING(FECHA_DE_NACIMIENTO,7,4),SUBSTRING(FECHA_DE_NACIMIENTO,1,2),SUBSTRING(FECHA_DE_NACIMIENTO,4,2)))) > GETDATE() THEN 1
           ELSE 0
       END AS EDAD
      ,CONVERT(float, SALDO_ACTUAL)*1.0 AS SALDO
  FROM stg.ZGM_CONC_SLD;


--SALDO_MC
--SAL_TJ_MC
TRUNCATE TABLE stg.SALDO_MC;

INSERT INTO stg.SALDO_MC
SELECT smc.CODIGOSEGUIMIENTO
      ,smc.INSTITUCION
      ,smc.EMPRESA
      ,smc.CLIENTE
      ,smc.NROTARJETA
      ,smc.FECPROCESO
      ,smc.MONEDA
      ,smc.SALDOARCHIVOUBA
      ,smc.SALDOSIMP
      ,smc.ESTADO
      ,smc.IDCUENTA
      ,smc.BIN
      ,smc.SUBBIN
      ,CONCAT(SUBSTRING(smc.FECPROCESO,7,4),'.',SUBSTRING(smc.FECPROCESO,1,2)) AS MES
      ,CONCAT(SUBSTRING(smc.FECPROCESO,7,4),'.',SUBSTRING(smc.FECPROCESO,1,2),'.',SUBSTRING(smc.FECPROCESO,4,2)) AS DIA
 FROM stg.ZGM_SAL_TJ_MC smc;


--COMPRAS
--SWDM
TRUNCATE TABLE stg.COMPRAS;

INSERT INTO stg.COMPRAS
SELECT swdm.MESSAGE_TYPE
      ,swdm.IND_STORE_FORWARD
      ,swdm.ACCOUNT_NUMBER
      ,swdm.PROCESSING_CODE
      ,swdm.TRANSACTION_CURRENCY
      ,swdm.TRANSACTION_AMOUNT
      ,swdm.SETTLEMENT_CURRENCY
      ,swdm.SETTLEMENT_AMOUNT
      ,swdm.BILLING_CURRENCY
      ,swdm.CARD_ISSUER_AMOUNT
      ,swdm.TRANSMISSION_DATE
      ,swdm.TRANSMISSION_TIME
      ,swdm.CONVERSION_RATE_SETTLEMENT
      ,swdm.CONVERSION_RATE_BILLING
      ,swdm.TRACENUMBER
      ,swdm.LOCAL_TRANSACTION_TIME
      ,swdm.LOCAL_TRANSACTION_DATE
      ,swdm.EXPIRY_DATE
      ,swdm.SETLLEMENT_DATE
      ,swdm.CONVERSION_DATE
      ,swdm.CAPTURE_DATE
      ,swdm.MERCHANT_TYPE
      ,swdm.POS_ENTRY_MODE
      ,swdm.NETWORK_IDENTIFIER
      ,swdm.POS_CONDITION_CODE
      ,swdm.SETT_AMOUNT_TRANSACTION_FEE
      ,swdm.SETT_AMOUNT_PROCESSING_FEE
      ,swdm.ACQUIRING_INSTITUTION
      ,swdm.FORWARDING_INSTITUTION
      ,swdm.TRACK2_DATA
      ,swdm.REFERENCE_NUMBER
      ,swdm.AUTHORIZATION_CODE
      ,swdm.RESPONSE_CODE
      ,swdm.CARD_ACCEPTOR_TERMID
      ,swdm.CARD_ACCEPTOR_LOCATION
      ,swdm.ORIGINAL_MESSAGE_TYPE
      ,swdm.ORIGINAL_TRACE_NUMBER
      ,swdm.ORIGINAL_DATE
      ,swdm.ORIGINAL_TIME
      ,swdm.ORIGINAL_ADQUIRING_INST
      ,swdm.ORIGINAL_FORWARDING_INST
      ,swdm.REPLA_TRANSACTION_AMOUNT
      ,swdm.REPLA_SETTLEMENT_AMOUNT
      ,swdm.REPLA_TRANSACTION_FEE
      ,swdm.REPLA_SETTLEMENT_FEE
      ,swdm.MERCHANT
      ,swdm.REQUESTING_INSTITUTION
      ,swdm.ACCOUNT_IDENTIFICATION1
      ,swdm.ACCOUNT_IDENTIFICATION2
      ,swdm.CARDC_ATEGORY
      ,swdm.TIERII
      ,swdm.CARD_ACCEPTOR_ID
      ,swdm.CONCILIA_AUTORIZACION
      ,swdm.CONCILIA_LOG_CONTABLE
      ,swdm.ROL_TRANSACCION
      ,swdm.ID_CLASE_SERVICIO
      ,swdm.ID_MEMBRESIA
      ,swdm.ID_ORIGEN
      ,swdm.ID_CODIGO_TRANSACCION
      ,swdm.ID_CLASE_TRANSACCION
      ,swdm.FECHA_PROCESO
      ,swdm.ID_CLIENTE
      ,swdm.ID_EMPRESA
      ,swdm.ID_PROCESO
      ,swdm.ID_CANAL
      ,swdm.ID_BIN
      ,swdm.CUENTA_FROM
      ,swdm.CUENTA_TO
      ,swdm.NUMERO_DOCUMENTO--viene vacio
      ,swdm.CUENTA_CARGO
      ,swdm.CUENTA_ABONO
      ,swdm.CODIGO_ANALITICO
      ,swdm.CONTAB_FONDOS
      ,swdm.ID_ATM
      ,swdm.FECHA_CONCILIA_LOG
      ,swdm.ID_SUB_BIN
      ,swdm.ID_INSTITUCION
      ,swdm.PAN_ENTRY_MODE
      ,swdm.PIN_ENTRY_CAPABILITY
      ,swdm.INSTITUCION_EMISORA
      ,swdm.INSTITUCION_RECEPTORA
      ,swdm.CODIGO_SEGUIMIENTO AS CODIGOSEGUIMIENTO
      ,swdm.TARJETA_PRESENTE
      ,SUBSTRING(swdm.Local_Transaction_Date,9,2) AS DIA
      ,CONCAT(SUBSTRING(swdm.Local_Transaction_Date,1,4),SUBSTRING(swdm.Local_Transaction_Date,6,2)) AS PERIODO
      ,IIF(CHARINDEX ('LIMA', swdm.Card_Acceptor_Location)=0, swdm.Card_Acceptor_Location, TRIM(SUBSTRING(swdm.Card_Acceptor_Location, 1, CHARINDEX ('LIMA', swdm.Card_Acceptor_Location)-1))) AS SELLER
      ,IIF(TRIM(UPPER(swdm.Tarjeta_Presente))='SI' OR swdm.Card_Acceptor_TermId = '00000003','Presencial','On line') AS TIPO
      ,IIF(SUBSTRING(swdm.Message_Type,1,4)='0400',CONVERT(float, swdm.Transaction_Amount)*-1.0,CONVERT(float, swdm.Transaction_Amount)*1.0) AS MONTO
      ,IIF(SUBSTRING(swdm.Message_Type,1,4)='0400',-1,1) AS CONTADOR
  FROM stg.ZGM_SWDM swdm;


--MOVIMIENTOS
--CONC_MOV
TRUNCATE TABLE stg.MOVIMIENTOS;

INSERT INTO stg.MOVIMIENTOS
SELECT gcm.TRANSACTION_ID,
       RIGHT(CONCAT('000000000',gcm.MOBILE_NUMBER),9) AS CELULAR,
       CONCAT(SUBSTRING(gcm.DATE,7,4),SUBSTRING(gcm.DATE,1,2),SUBSTRING(gcm.DATE,4,2)) AS DATE,
       CONVERT(float, gcm.AMOUNT)*1.0 AS AMOUNT,
       CONVERT(float, gcm.COMMISSION)*1.0 AS COMMISSION,
       IIF(gcm.MERCHANT_CODE IS NULL,
           IIF(gcm.DESTINATION_MOBILE IS NULL,
               'A tarjeta',   
               IIF(SUBSTRING(gcm.DESTINATION_MOBILE,3,1) <> '0',
                   'A billetera',
                   IIF(LEFT(gcm.DESTINATION_MOBILE,2) = '51',
                       'A BCP',
                       'Prymera')
                  )
              ),
           'Locatario') AS ACTOR,
       gcm.TRANSACTION_TYPE,
       CASE gcm.TRANSACTION_TYPE
         WHEN '1' THEN 'Cash Out'
         WHEN '2' THEN 'Cash In'
         WHEN '3' THEN 'Transferencia'
         WHEN '7' THEN 'PgoServicios'
         WHEN '8' THEN 'Recarga Celular'
         WHEN '32' THEN 'AJUSTES'
         ELSE 'NULL'
       END AS TIPO,
       IIF(gcm.MERCHANT_CODE IS NULL,
           IIF(gcm.DESTINATION_MOBILE IS NULL,
               'Interno',   
               'Externo'
              ),
           'Externo') AS AMBITO,
       CONCAT(SUBSTRING(DATE,7,4),SUBSTRING(DATE,1,2)) AS PERIODO
  FROM stg.ZGM_CONC_MOV gcm;


--CLIENTE
--CUST
TRUNCATE TABLE stg.CLIENTE;

INSERT INTO stg.CLIENTE
SELECT CONCAT(IIF(gc.document_type = 'Documento Nacional de Identidad',  RIGHT(CONCAT('00000000',TRIM(gc.DOCUMENT_NUMBER)),8),NULL),RIGHT(CONCAT('000000000',gc.CELLPHONE),9)) as DNI_CELULAR
      ,CASE
         WHEN gc.document_type IS NULL THEN NULL
         WHEN gc.document_type = 'Documento Nacional de Identidad' THEN 'DNI'
         ELSE 'OTRO'
       END AS TIPODOCUMENTO
      ,IIF(gc.document_type = 'Documento Nacional de Identidad',  RIGHT(CONCAT('00000000',TRIM(gc.DOCUMENT_NUMBER)),8),NULL) AS DNI
      ,RIGHT(CONCAT('000000000',gc.CELLPHONE),9) AS CELULAR
      ,gc.FIRST_NAME
      ,gc.SECOND_NAME
      ,gc.THIRD_NAME
      ,gc.FOURTH_NAME
      ,gc.FIFTH_NAME
      ,gc.FIRST_SURNAME
      ,gc.SECOND_SURNAME
      ,gc.EMAIL
      ,gc.AFFILIATE_CODE
      ,gc.STATUS
      ,gc.HABILITADO_RECARGA
      ,gc.SEGMENTO
      ,CONCAT(SUBSTRING(RIGHT(CONCAT('0',birth_date),19),7,4),SUBSTRING(RIGHT(CONCAT('0',birth_date),19),1,2),SUBSTRING(RIGHT(CONCAT('0',birth_date),19),4,2)) AS FECNACIMIENTO
      ,DATEDIFF(YY, CONVERT(DATETIME, CONCAT(SUBSTRING(RIGHT(CONCAT('0',birth_date),19),7,4),SUBSTRING(RIGHT(CONCAT('0',birth_date),19),1,2),SUBSTRING(RIGHT(CONCAT('0',birth_date),19),4,2))), GETDATE()) -
       CASE 
           WHEN DATEADD(YY,DATEDIFF(YY, CONVERT(DATETIME, CONCAT(SUBSTRING(RIGHT(CONCAT('0',birth_date),19),7,4),SUBSTRING(RIGHT(CONCAT('0',birth_date),19),1,2),SUBSTRING(RIGHT(CONCAT('0',birth_date),19),4,2))), GETDATE()),CONVERT(DATETIME, CONCAT(SUBSTRING(RIGHT(CONCAT('0',birth_date),19),7,4),SUBSTRING(RIGHT(CONCAT('0',birth_date),19),1,2),SUBSTRING(RIGHT(CONCAT('0',birth_date),19),4,2)))) > GETDATE() THEN 1
           ELSE 0
       END AS EDAD
      ,CONCAT(SUBSTRING(gc.created_at,7,4),SUBSTRING(gc.created_at,1,2),SUBSTRING(gc.created_at,4,2)) AS FECCREACION
      ,SUBSTRING(gc.created_at,12,8) AS HORACREACION
      ,SUBSTRING(gc.created_at,12,2) AS HORAHHCREACION
      ,IIF(gc.affiliate_code IS NULL,'NO','SI') AS AFILIADOR
      ,CASE
         WHEN gc.affiliate_code IS NULL THEN 'Otros'
         WHEN LEFT(gc.affiliate_code,1) = 'S' THEN 'Mall del Sur'
         WHEN LEFT(gc.affiliate_code,1) = 'P' THEN 'Plaza Norte'
         ELSE 'Otros'
       END AS AFILIADO
      ,CASE
         WHEN gc.gender IS NULL THEN 'Sin definir'
         WHEN gc.gender = '1' THEN 'Masculino'
         WHEN gc.gender = '0' THEN 'Femenino'
         ELSE 'Sin definir'
       END AS GENERO
      ,CONCAT(SUBSTRING(gc.created_at,7,4),SUBSTRING(gc.created_at,1,2)) AS PER_CREACION_CLIENTE
  FROM stg.ZGM_CUST gc;


--ods_&_______________________________________________________________________________________________


--SALDO_BE
TRUNCATE TABLE ods.SALDO_BE;

WITH t AS(
SELECT DNI
      ,FEC_ACTUALIZACION
      ,TIPODOCUMENTO
      ,CELULAR
      ,DNI_CELULAR
      ,FECNACIMIENTO
      ,EDAD
      ,SALDO
      ,ROW_NUMBER() OVER (PARTITION BY DNI ORDER BY FEC_ACTUALIZACION DESC) as ORDCREACION
  FROM stg.SALDO_BE)
INSERT INTO ods.SALDO_BE
SELECT DNI
      ,FEC_ACTUALIZACION
      ,TIPODOCUMENTO
      ,CELULAR
      ,DNI_CELULAR
      ,FECNACIMIENTO
      ,EDAD
      ,SALDO
  FROM t
 WHERE ORDCREACION = 1;


--SALDO_MC
TRUNCATE TABLE ods.SALDO_MC;

INSERT INTO ods.SALDO_MC
SELECT gmt.DNI
      ,SUM(smc.SALDOSIMP) AS SALDOSIMP
  FROM stg.SALDO_MC smc
       LEFT JOIN stg.TARJETA gmt ON smc.CODIGOSEGUIMIENTO = gmt.CODIGOSEGUIMIENTO
 WHERE smc.DIA = (SELECT MAX(DIA) FROM stg.SALDO_MC)
 GROUP BY gmt.DNI;


--MOVIMIENTOS
TRUNCATE TABLE ods.MOVIMIENTOS_T1;

INSERT INTO ods.MOVIMIENTOS_T1
SELECT m.*
      ,ca.COD_CANAL
  FROM stg.MOVIMIENTOS m
       LEFT JOIN stg.ZGM_CONC_CNL ca ON m.TRANSACTION_ID = ca.REF_NUMBER;

--DROP TABLE bds.MOVIMIENTOS;
TRUNCATE TABLE ods.MOVIMIENTOS_T2;

DROP INDEX MOVIMIENTOS_T2_I1 ON ods.MOVIMIENTOS_T2;

INSERT INTO ods.MOVIMIENTOS_T2
SELECT m.TRANSACTION_ID
      ,m.CELULAR
      ,m.DATE
      ,m.AMOUNT
      ,m.COMMISSION
      ,m.ACTOR
      ,m.TIPO
      ,m.AMBITO
      ,m.PERIODO
      ,m.COD_CANAL
      ,m.TRANSACTION_TYPE
      ,CASE WHEN m.COD_CANAL = '4'   AND m.transaction_type = '1'  THEN AMOUNT ELSE 0 END AS KasNet_CashOut
      ,CASE WHEN m.COD_CANAL = '4'   AND m.transaction_type = '2'  THEN AMOUNT ELSE 0 END AS KasNet_CashIn
      ,CASE WHEN m.COD_CANAL = '8'   AND m.transaction_type = '2'  THEN AMOUNT ELSE 0 END AS BCP_CashIn
      ,CASE WHEN m.COD_CANAL = '9'   AND m.transaction_type = '1'  THEN AMOUNT ELSE 0 END AS Prymera_CashOut
      ,CASE WHEN m.COD_CANAL = '9'   AND m.transaction_type = '2'  THEN AMOUNT ELSE 0 END AS Prymera_CashIn
      ,CASE WHEN m.COD_CANAL = '11'  AND m.transaction_type = '1'  THEN AMOUNT ELSE 0 END AS A_tarjeta_Envio
      ,CASE WHEN m.COD_CANAL = '11'  AND m.transaction_type = '2'  THEN AMOUNT ELSE 0 END AS A_tarjeta_Recupero
      ,CASE WHEN m.COD_CANAL = '12'  AND m.transaction_type = '2'  THEN AMOUNT ELSE 0 END AS SCB_CashIn
      ,CASE WHEN m.COD_CANAL IS NULL AND m.transaction_type = '3'  THEN AMOUNT ELSE 0 END AS BE_Transferencia
      ,CASE WHEN m.COD_CANAL IS NULL AND m.transaction_type = '7'  THEN AMOUNT ELSE 0 END AS BE_PgoServicios
      ,CASE WHEN m.COD_CANAL IS NULL AND m.transaction_type = '8'  THEN AMOUNT ELSE 0 END AS BE_RecargaCelular
      ,CASE WHEN m.COD_CANAL IS NULL AND m.transaction_type = '32' THEN AMOUNT ELSE 0 END AS BE_AJUSTES
      ,ROW_NUMBER() OVER (PARTITION BY CELULAR ORDER BY DATE DESC) as ORDDATEDESC
  FROM ods.MOVIMIENTOS_T1 m
 WHERE m.TIPO <> 'Cash Out'
  UNION ALL
SELECT m.TRANSACTION_ID
      ,m.CELULAR
      ,m.DATE
      ,m.AMOUNT
      ,m.COMMISSION
      ,m.ACTOR
      ,m.TIPO
      ,m.AMBITO
      ,m.PERIODO
      ,m.COD_CANAL
      ,m.TRANSACTION_TYPE
      ,CASE WHEN m.COD_CANAL = '4'   AND m.transaction_type = '1'  THEN AMOUNT ELSE 0 END AS KasNet_CashOut
      ,CASE WHEN m.COD_CANAL = '4'   AND m.transaction_type = '2'  THEN AMOUNT ELSE 0 END AS KasNet_CashIn
      ,CASE WHEN m.COD_CANAL = '8'   AND m.transaction_type = '2'  THEN AMOUNT ELSE 0 END AS BCP_CashIn
      ,CASE WHEN m.COD_CANAL = '9'   AND m.transaction_type = '1'  THEN AMOUNT ELSE 0 END AS Prymera_CashOut
      ,CASE WHEN m.COD_CANAL = '9'   AND m.transaction_type = '2'  THEN AMOUNT ELSE 0 END AS Prymera_CashIn
      ,CASE WHEN m.COD_CANAL = '11'  AND m.transaction_type = '1'  THEN AMOUNT ELSE 0 END AS A_tarjeta_Envio
      ,CASE WHEN m.COD_CANAL = '11'  AND m.transaction_type = '2'  THEN AMOUNT ELSE 0 END AS A_tarjeta_Recupero
      ,CASE WHEN m.COD_CANAL = '12'  AND m.transaction_type = '2'  THEN AMOUNT ELSE 0 END AS SCB_CashIn
      ,CASE WHEN m.COD_CANAL IS NULL AND m.transaction_type = '3'  THEN AMOUNT ELSE 0 END AS BE_Transferencia
      ,CASE WHEN m.COD_CANAL IS NULL AND m.transaction_type = '7'  THEN AMOUNT ELSE 0 END AS BE_PgoServicios
      ,CASE WHEN m.COD_CANAL IS NULL AND m.transaction_type = '8'  THEN AMOUNT ELSE 0 END AS BE_RecargaCelular
      ,CASE WHEN m.COD_CANAL IS NULL AND m.transaction_type = '32' THEN AMOUNT ELSE 0 END AS BE_AJUSTES
      ,0 as ORDDATEDESC
  FROM ods.MOVIMIENTOS_T1 m
 WHERE m.TIPO = 'Cash Out'
  ;

CREATE INDEX MOVIMIENTOS_T2_I1 ON ods.MOVIMIENTOS_T2 (CELULAR);

TRUNCATE TABLE ods.MOVIMIENTOS_T3;

DROP INDEX MOVIMIENTOS_T3_I1 ON ods.MOVIMIENTOS_T3;

INSERT INTO ods.MOVIMIENTOS_T3
SELECT t.CELULAR 
      ,SUM(COMMISSION) AS COMMISSION
      ,SUM(t.KasNet_CashOut    ) AS KasNet_CashOut
      ,SUM(t.KasNet_CashIn     ) AS KasNet_CashIn
      ,SUM(t.BCP_CashIn        ) AS BCP_CashIn
      ,SUM(t.Prymera_CashOut   ) AS Prymera_CashOut
      ,SUM(t.Prymera_CashIn    ) AS Prymera_CashIn
      ,SUM(t.A_tarjeta_Envio   ) AS A_tarjeta_Envio
      ,SUM(t.A_tarjeta_Recupero) AS A_tarjeta_Recupero
      ,SUM(t.SCB_CashIn        ) AS SCB_CashIn
      ,SUM(t.BE_Transferencia  ) AS BE_Transferencia
      ,SUM(t.BE_PgoServicios   ) AS BE_PgoServicios
      ,SUM(t.BE_RecargaCelular ) AS BE_RecargaCelular
      ,SUM(t.BE_AJUSTES        ) AS BE_AJUSTES
      ,SUM(IIF(t.KasNet_CashOut    >0,1,IIF(t.KasNet_CashOut    <0,-1,0))) AS count_KasNet_CashOut
      ,SUM(IIF(t.KasNet_CashIn     >0,1,IIF(t.KasNet_CashIn     <0,-1,0))) AS count_KasNet_CashIn
      ,SUM(IIF(t.BCP_CashIn        >0,1,IIF(t.BCP_CashIn        <0,-1,0))) AS count_BCP_CashIn
      ,SUM(IIF(t.Prymera_CashOut   >0,1,IIF(t.Prymera_CashOut   <0,-1,0))) AS count_Prymera_CashOut
      ,SUM(IIF(t.Prymera_CashIn    >0,1,IIF(t.Prymera_CashIn    <0,-1,0))) AS count_Prymera_CashIn
      ,SUM(IIF(t.A_tarjeta_Envio   >0,1,IIF(t.A_tarjeta_Envio   <0,-1,0))) AS count_A_tarjeta_Envio
      ,SUM(IIF(t.A_tarjeta_Recupero>0,1,IIF(t.A_tarjeta_Recupero<0,-1,0))) AS count_A_tarjeta_Recupero
      ,SUM(IIF(t.SCB_CashIn        >0,1,IIF(t.SCB_CashIn        <0,-1,0))) AS count_SCB_CashIn
      ,SUM(IIF(t.BE_Transferencia  >0,1,IIF(t.BE_Transferencia  <0,-1,0))) AS count_BE_Transferencia
      ,SUM(IIF(t.BE_PgoServicios   >0,1,IIF(t.BE_PgoServicios   <0,-1,0))) AS count_BE_PgoServicios
      ,SUM(IIF(t.BE_RecargaCelular >0,1,IIF(t.BE_RecargaCelular <0,-1,0))) AS count_BE_RecargaCelular
      ,SUM(IIF(t.BE_AJUSTES        >0,1,IIF(t.BE_AJUSTES        <0,-1,0))) AS count_BE_AJUSTES
      ,NULL AS FECULT_CI
  FROM ods.MOVIMIENTOS_T2 t
 GROUP BY t.CELULAR;

CREATE INDEX MOVIMIENTOS_T3_I1 ON ods.MOVIMIENTOS_T3 (CELULAR);

UPDATE t
   SET t.FECULT_CI = m.DATE
  FROM ods.MOVIMIENTOS_T3 t
       INNER JOIN ods.MOVIMIENTOS_T2 m
               ON m.CELULAR = t.CELULAR
              AND m.ORDDATEDESC = 1;

--COMPRAS
TRUNCATE TABLE ods.COMPRAS;

INSERT INTO ods.COMPRAS
SELECT c.MESSAGE_TYPE
      ,c.IND_STORE_FORWARD
      ,c.ACCOUNT_NUMBER
      ,c.PROCESSING_CODE
      ,c.TRANSACTION_CURRENCY
      ,c.TRANSACTION_AMOUNT
      ,c.SETTLEMENT_CURRENCY
      ,c.SETTLEMENT_AMOUNT
      ,c.BILLING_CURRENCY
      ,c.CARD_ISSUER_AMOUNT
      ,c.TRANSMISSION_DATE
      ,c.TRANSMISSION_TIME
      ,c.CONVERSION_RATE_SETTLEMENT
      ,c.CONVERSION_RATE_BILLING
      ,c.TRACENUMBER
      ,c.LOCAL_TRANSACTION_TIME
      ,c.LOCAL_TRANSACTION_DATE
      ,c.EXPIRY_DATE
      ,c.SETLLEMENT_DATE
      ,c.CONVERSION_DATE
      ,c.CAPTURE_DATE
      ,c.MERCHANT_TYPE
      ,c.POS_ENTRY_MODE
      ,c.NETWORK_IDENTIFIER
      ,c.POS_CONDITION_CODE
      ,c.SETT_AMOUNT_TRANSACTION_FEE
      ,c.SETT_AMOUNT_PROCESSING_FEE
      ,c.ACQUIRING_INSTITUTION
      ,c.FORWARDING_INSTITUTION
      ,c.TRACK2_DATA
      ,c.REFERENCE_NUMBER
      ,c.AUTHORIZATION_CODE
      ,c.RESPONSE_CODE
      ,c.CARD_ACCEPTOR_TERMID
      ,c.CARD_ACCEPTOR_LOCATION
      ,c.ORIGINAL_MESSAGE_TYPE
      ,c.ORIGINAL_TRACE_NUMBER
      ,c.ORIGINAL_DATE
      ,c.ORIGINAL_TIME
      ,c.ORIGINAL_ADQUIRING_INST
      ,c.ORIGINAL_FORWARDING_INST
      ,c.REPLA_TRANSACTION_AMOUNT
      ,c.REPLA_SETTLEMENT_AMOUNT
      ,c.REPLA_TRANSACTION_FEE
      ,c.REPLA_SETTLEMENT_FEE
      ,c.MERCHANT
      ,c.REQUESTING_INSTITUTION
      ,c.ACCOUNT_IDENTIFICATION1
      ,c.ACCOUNT_IDENTIFICATION2
      ,c.CARDC_ATEGORY
      ,c.TIERII
      ,c.CARD_ACCEPTOR_ID
      ,c.CONCILIA_AUTORIZACION
      ,c.CONCILIA_LOG_CONTABLE
      ,c.ROL_TRANSACCION
      ,c.ID_CLASE_SERVICIO
      ,c.ID_MEMBRESIA
      ,c.ID_ORIGEN
      ,c.ID_CODIGO_TRANSACCION
      ,c.ID_CLASE_TRANSACCION
      ,c.FECHA_PROCESO
      ,c.ID_CLIENTE
      ,c.ID_EMPRESA
      ,c.ID_PROCESO
      ,c.ID_CANAL
      ,c.ID_BIN
      ,c.CUENTA_FROM
      ,c.CUENTA_TO
      ,c.NUMERO_DOCUMENTO
      ,c.CUENTA_CARGO
      ,c.CUENTA_ABONO
      ,c.CODIGO_ANALITICO
      ,c.CONTAB_FONDOS
      ,c.ID_ATM
      ,c.FECHA_CONCILIA_LOG
      ,c.ID_SUB_BIN
      ,c.ID_INSTITUCION
      ,c.PAN_ENTRY_MODE
      ,c.PIN_ENTRY_CAPABILITY
      ,c.INSTITUCION_EMISORA
      ,c.INSTITUCION_RECEPTORA
      ,c.CODIGOSEGUIMIENTO
      ,c.TARJETA_PRESENTE
      ,c.DIA
      ,c.PERIODO
      ,c.SELLER
      ,c.TIPO
      ,c.MONTO
      ,c.CONTADOR
      ,RIGHT(tj.CELULAR,9) AS CELULAR
      ,tj.DNI
      ,CONCAT(tj.DNI,RIGHT(tj.CELULAR,9)) as DNI_CELULAR
      ,CONCAT(tj.NOMBRES, ' ', tj.APELLIDO_PATERNO, ' ', tj.APELLIDO_MATERNO) AS NOMBRE
      ,CASE
        WHEN Response_Code = '0 - NORMAL, NO ERROR' AND Id_Codigo_Transaccion LIKE  '10%' THEN 'COMPRA VALIDA'
        WHEN Id_Codigo_Transaccion like '94%' THEN 'TRANSFERENCIA'
        WHEN Id_Codigo_Transaccion like '13%' THEN 'COMPRA RECHAZADA'
        ELSE NULL
       END AS RESULTADO_COMPRA
  FROM stg.COMPRAS c
       LEFT JOIN stg.TARJETA tj ON c.CODIGOSEGUIMIENTO = tj.CODIGOSEGUIMIENTO;


--CLIENTE
TRUNCATE TABLE ods.CLIENTE_T1;

WITH t AS(
SELECT c.DNI_CELULAR
      ,c.TIPODOCUMENTO
      ,c.DNI
      ,c.CELULAR
      ,c.FIRST_NAME
      ,c.SECOND_NAME
      ,c.THIRD_NAME
      ,c.FOURTH_NAME
      ,c.FIFTH_NAME
      ,c.FIRST_SURNAME
      ,c.SECOND_SURNAME
      ,c.EMAIL
      ,c.AFFILIATE_CODE
      ,c.STATUS
      ,c.HABILITADO_RECARGA
      ,c.SEGMENTO
      ,c.FECNACIMIENTO
      ,c.EDAD
      ,c.FECCREACION
      ,c.HORACREACION
      ,c.HORAHHCREACION
      ,c.AFILIADOR
      ,c.AFILIADO
      ,c.GENERO
      ,c.PER_CREACION_CLIENTE
      ,ROW_NUMBER() OVER (PARTITION BY c.DNI, c.CELULAR ORDER BY c.STATUS, c.HABILITADO_RECARGA DESC, c.FECCREACION DESC, c.HORACREACION DESC) as ORDCREACION
  FROM stg.CLIENTE c)
INSERT INTO ods.CLIENTE_T1
SELECT DNI_CELULAR
      ,TIPODOCUMENTO
      ,DNI
      ,CELULAR
      ,FIRST_NAME
      ,SECOND_NAME
      ,THIRD_NAME
      ,FOURTH_NAME
      ,FIFTH_NAME
      ,FIRST_SURNAME
      ,SECOND_SURNAME
      ,EMAIL
      ,AFFILIATE_CODE
      ,STATUS
      ,HABILITADO_RECARGA
      ,SEGMENTO
      ,FECNACIMIENTO
      ,EDAD
      ,FECCREACION
      ,HORACREACION
      ,HORAHHCREACION
      ,AFILIADOR
      ,AFILIADO
      ,GENERO
      ,PER_CREACION_CLIENTE
  FROM t
 WHERE ORDCREACION = 1;

TRUNCATE TABLE ods.CLIENTE_T2;

INSERT INTO ods.CLIENTE_T2
SELECT c.DNI_CELULAR
      ,c.TIPODOCUMENTO
      ,c.DNI
      ,c.CELULAR
      ,c.FIRST_NAME
      ,c.SECOND_NAME
      ,c.THIRD_NAME
      ,c.FOURTH_NAME
      ,c.FIFTH_NAME
      ,c.FIRST_SURNAME
      ,c.SECOND_SURNAME
      ,c.EMAIL
      ,c.AFFILIATE_CODE
      ,c.STATUS
      ,c.HABILITADO_RECARGA
      ,c.SEGMENTO
      ,c.FECNACIMIENTO
      ,c.EDAD
      ,c.FECCREACION
      ,c.HORACREACION
      ,c.HORAHHCREACION
      ,c.AFILIADOR
      ,c.AFILIADO
      ,c.GENERO
      ,c.PER_CREACION_CLIENTE
      ,IIF(be.SALDO IS NULL,0,be.SALDO) AS SALDOBE
      ,IIF(mc.SALDOSIMP IS NULL,0,mc.SALDOSIMP) AS SALDOMC
  FROM ods.CLIENTE_T1 c
       LEFT JOIN ods.SALDO_BE be
            ON c.DNI_CELULAR = be.DNI_CELULAR
       LEFT JOIN ods.SALDO_MC mc
            ON c.DNI = mc.DNI;

TRUNCATE TABLE ods.CLIENTE_T3;

WITH t AS (
SELECT DNI_CELULAR,
       RESULTADO_COMPRA,
       SUM(MONTO) AS MONTO
  FROM ods.COMPRAS
 GROUP BY DNI_CELULAR, RESULTADO_COMPRA)
INSERT INTO ods.CLIENTE_T3
SELECT c.DNI_CELULAR
      ,c.TIPODOCUMENTO
      ,c.DNI
      ,c.CELULAR
      ,c.FIRST_NAME
      ,c.SECOND_NAME
      ,c.THIRD_NAME
      ,c.FOURTH_NAME
      ,c.FIFTH_NAME
      ,c.FIRST_SURNAME
      ,c.SECOND_SURNAME
      ,c.EMAIL
      ,c.AFFILIATE_CODE
      ,c.STATUS
      ,c.HABILITADO_RECARGA
      ,c.SEGMENTO
      ,c.FECNACIMIENTO
      ,c.EDAD
      ,c.FECCREACION
      ,c.HORACREACION
      ,c.HORAHHCREACION
      ,c.AFILIADOR
      ,c.AFILIADO
      ,c.GENERO
      ,c.PER_CREACION_CLIENTE
      ,c.SALDOBE
      ,c.SALDOMC
      ,CASE WHEN t.RESULTADO_COMPRA = 'COMPRA VALIDA'    THEN t.MONTO ELSE 0 END AS MTO_COMPRA_VALIDA
      ,CASE WHEN t.RESULTADO_COMPRA = 'TRANSFERENCIA'    THEN t.MONTO ELSE 0 END AS MTO_TRANSFERENCIA
      ,CASE WHEN t.RESULTADO_COMPRA = 'COMPRA RECHAZADA' THEN t.MONTO ELSE 0 END AS MTO_COMPRA_RECHZD
  FROM ods.CLIENTE_T2 c
       LEFT JOIN t ON c.DNI_CELULAR = t.DNI_CELULAR;

--bds_________________________________________________________________________________________________

TRUNCATE TABLE bds.Q_CLIENTE;

INSERT INTO bds.Q_CLIENTE
SELECT c.DNI_CELULAR
      ,c.TIPODOCUMENTO AS "Tipo documento"
      ,c.DNI
      ,c.CELULAR AS "Celular"
      ,c.FIRST_NAME AS "Primer nombre"
      ,c.SECOND_NAME AS "Segundo nombre"
      ,c.THIRD_NAME AS "Tercer nombre"
      ,c.FOURTH_NAME AS "Cuarto nombre"
      ,c.FIFTH_NAME AS "Quinto nombre"
      ,c.FIRST_SURNAME AS "Primer apellido"
      ,c.SECOND_SURNAME AS "Segundo apellido"
      ,c.EMAIL AS "Correo electrónico"
      ,c.AFFILIATE_CODE
      ,c.STATUS AS "Status"
      ,c.HABILITADO_RECARGA AS "Habilitado recarga"
      ,c.SEGMENTO AS "Segmento"
      ,c.FECNACIMIENTO AS "Fecha nacimiento"
      ,c.EDAD AS "Edad"
      ,c.FECCREACION AS "Fec. Cre. Cli."
      ,c.HORACREACION AS "Hora Cre. Cli."
      ,c.HORAHHCREACION AS "Hora HH Cre. Cli."
      ,c.AFILIADOR AS "Afiliador"
      ,c.AFILIADO AS "Afiliado"
      ,c.GENERO AS "Genero"
      ,c.PER_CREACION_CLIENTE AS "Periodo Cre. Cli."
      ,c.SALDOBE AS "B.E. Saldo"
      ,c.SALDOMC AS "MC Saldo"
      ,IIF(c.MTO_COMPRA_VALIDA IS NULL, 0, c.MTO_COMPRA_VALIDA) AS "Mto. Compra válida"
      ,IIF(c.MTO_TRANSFERENCIA IS NULL, 0, c.MTO_TRANSFERENCIA) AS "Mto. Transferencia"
      ,IIF(c.MTO_COMPRA_RECHZD IS NULL, 0, c.MTO_COMPRA_RECHZD) AS "Mto. Compra rechazada"
      ,IIF(c.MTO_COMPRA_VALIDA IS NULL, 'No', 'Sí') AS "Cliente Compra"
      ,m.FECULT_CI AS "Fec. Ult. CashIn"
      ,ISNULL((SELECT 'Sí'
                 FROM MD_W_CLB.stg.USERS b
                WHERE RIGHT(CONCAT('00000000',TRIM(b.DOCUMENT_NUMBER)),8) = c.DNI)
               , 'No') AS "Con APP Beneficios"
      , IIF(m.FECULT_CI IS NULL, IIF(c.HABILITADO_RECARGA = 1, 'Validado', 'Inscrito')  ,'Recargado') AS "Etapa"
      ,m.COMMISSION                AS "Mto.Mov. Comisión"
      ,m.KasNet_CashOut            AS "Mto.Mov. KasNet_CO"
      ,m.KasNet_CashIn             AS "Mto.Mov. KasNet_CI"
      ,m.BCP_CashIn                AS "Mto.Mov. BCP_CI"
      ,m.Prymera_CashOut           AS "Mto.Mov. Prymera_CO"
      ,m.Prymera_CashIn            AS "Mto.Mov. Prymera_CI"
      ,m.A_tarjeta_Envio           AS "Mto.Mov. TarjEnvío"
      ,m.A_tarjeta_Recupero        AS "Mto.Mov. TarjRecupero"
      ,m.SCB_CashIn                AS "Mto.Mov. SCOTIA_CI"
      ,m.BE_Transferencia          AS "Mto.Mov. B.E. Transf."
      ,m.BE_PgoServicios           AS "Mto.Mov. B.E. PagoServ"
      ,m.BE_RecargaCelular         AS "Mto.Mov. B.E. RecCel"
      ,m.BE_AJUSTES                AS "Mto.Mov. B.E. Ajustes"
      ,m.count_KasNet_CashOut      AS "Cnt.Mov. KasNet_CO"
      ,m.count_KasNet_CashIn       AS "Cnt.Mov. KasNet_CI"
      ,m.count_BCP_CashIn          AS "Cnt.Mov. BCP_CI"
      ,m.count_Prymera_CashOut     AS "Cnt.Mov. Prymera_CO"
      ,m.count_Prymera_CashIn      AS "Cnt.Mov. Prymera_CI"
      ,m.count_A_tarjeta_Envio     AS "Cnt.Mov. TarjEnvío"
      ,m.count_A_tarjeta_Recupero  AS "Cnt.Mov. TarjRecupero"
      ,m.count_SCB_CashIn          AS "Cnt.Mov. SCOTIA_CI"
      ,m.count_BE_Transferencia    AS "Cnt.Mov. B.E. Transf."
      ,m.count_BE_PgoServicios     AS "Cnt.Mov. B.E. PagoServ"
      ,m.count_BE_RecargaCelular   AS "Cant.Mov. B.E. RecCel"
      ,m.count_BE_AJUSTES          AS "Cnt.Mov. B.E. Ajustes"
  FROM ods.CLIENTE_T3 c
       LEFT JOIN ods.MOVIMIENTOS_T3 m ON c.CELULAR = m.CELULAR;


TRUNCATE TABLE bds.Q_MOVMNTS_CLIENTES;

INSERT INTO bds.Q_MOVMNTS_CLIENTES
SELECT mt.TRANSACTION_ID
--      ,mt.CELULAR
      ,mt.DATE AS "Fecha Movimiento"
      ,mt.ACTOR AS "Actor"
      ,mt.TIPO AS "Tipo"
      ,mt.AMBITO AS "Ámbito"
      ,mt.PERIODO AS "Periodo Movimiento"
      ,mt.COD_CANAL AS "Código canal"
      ,mt.TRANSACTION_TYPE
      ,mt.AMOUNT AS "Monto movimiento"
      ,mt.COMMISSION AS "Comisión movimiento"
      ,mt.KasNet_CashOut            AS "Mto.Mov. KasNet_CO"
      ,mt.KasNet_CashIn             AS "Mto.Mov. KasNet_CI"
      ,mt.BCP_CashIn                AS "Mto.Mov. BCP_CI"
      ,mt.Prymera_CashOut           AS "Mto.Mov. Prymera_CO"
      ,mt.Prymera_CashIn            AS "Mto.Mov. Prymera_CI"
      ,mt.A_tarjeta_Envio           AS "Mto.Mov. TarjEnvío"
      ,mt.A_tarjeta_Recupero        AS "Mto.Mov. TarjRecupero"
      ,mt.SCB_CashIn                AS "Mto.Mov. SCOTIA_CI"
      ,mt.BE_Transferencia          AS "Mto.Mov. B.E. Transf."
      ,mt.BE_PgoServicios           AS "Mto.Mov. B.E. PagoServ"
      ,mt.BE_RecargaCelular         AS "Mto.Mov. B.E. RecCel"
      ,mt.BE_AJUSTES                AS "Mto.Mov. B.E. Ajustes"
      ,qc.DNI_CELULAR
      ,qc.TIPODOCUMENTO AS "Tipo documento"
      ,qc.DNI
      ,qc.CELULAR AS "Celular"
      ,qc.FIRST_NAME AS "Primer nombre"
      ,qc.SECOND_NAME AS "Segundo nombre"
      ,qc.THIRD_NAME AS "Tercer nombre"
      ,qc.FOURTH_NAME AS "Cuarto nombre"
      ,qc.FIFTH_NAME AS "Quinto nombre"
      ,qc.FIRST_SURNAME AS "Primer apellido"
      ,qc.SECOND_SURNAME AS "Segundo apellido"
      ,qc.EMAIL AS "Correo electrónico"
      ,qc.AFFILIATE_CODE
      ,qc.STATUS AS "Status"
      ,qc.HABILITADO_RECARGA AS "Habilitado recarga"
      ,qc.SEGMENTO AS "Segmento"
      ,qc.FECNACIMIENTO AS "Fecha nacimiento"
      ,qc.EDAD AS "Edad"
      ,qc.FECCREACION AS "Fec. Cre. Cli."
      ,qc.HORACREACION AS "Hora Cre. Cli."
      ,qc.HORAHHCREACION AS "Hora HH Cre. Cli."
      ,qc.AFILIADOR AS "Afiliador"
      ,qc.AFILIADO AS "Afiliado"
      ,qc.GENERO AS "Genero"
      ,qc.PER_CREACION_CLIENTE AS "Periodo Cre. Cli."
      ,qc.SALDOBE AS "B.E. Saldo"
      ,qc.SALDOMC AS "MC Saldo"
  FROM ods.MOVIMIENTOS_T2 mt
       LEFT JOIN ods.CLIENTE_T2 qc ON mt.CELULAR = qc.CELULAR;


TRUNCATE TABLE bds.Q_COMPRAS_CLIENTES;

INSERT INTO bds.Q_COMPRAS_CLIENTES
SELECT c.MESSAGE_TYPE AS "Tipo mensaje"
--      ,c.IND_STORE_FORWARD
--      ,c.ACCOUNT_NUMBER
--      ,c.PROCESSING_CODE
--      ,c.TRANSACTION_CURRENCY
--      ,c.TRANSACTION_AMOUNT --c.MONTO
--      ,c.SETTLEMENT_CURRENCY
--      ,c.SETTLEMENT_AMOUNT
--      ,c.BILLING_CURRENCY
--      ,c.CARD_ISSUER_AMOUNT
--      ,c.TRANSMISSION_DATE
--      ,c.TRANSMISSION_TIME
--      ,c.CONVERSION_RATE_SETTLEMENT
--      ,c.CONVERSION_RATE_BILLING
--      ,c.TRACENUMBER
      ,c.LOCAL_TRANSACTION_TIME AS "Hora local Compra"
      ,c.LOCAL_TRANSACTION_DATE AS "Fecha local Compra"
--      ,c.EXPIRY_DATE
--      ,c.SETLLEMENT_DATE
--      ,c.CONVERSION_DATE
--      ,c.CAPTURE_DATE
--      ,c.MERCHANT_TYPE
--      ,c.POS_ENTRY_MODE
--      ,c.NETWORK_IDENTIFIER
--      ,c.POS_CONDITION_CODE
--      ,c.SETT_AMOUNT_TRANSACTION_FEE
--      ,c.SETT_AMOUNT_PROCESSING_FEE
--      ,c.ACQUIRING_INSTITUTION
--      ,c.FORWARDING_INSTITUTION
--      ,c.TRACK2_DATA
      ,c.REFERENCE_NUMBER
      ,c.AUTHORIZATION_CODE
      ,c.RESPONSE_CODE
      ,c.CARD_ACCEPTOR_TERMID
      ,c.CARD_ACCEPTOR_LOCATION
--      ,c.ORIGINAL_MESSAGE_TYPE
--      ,c.ORIGINAL_TRACE_NUMBER
--      ,c.ORIGINAL_DATE
--      ,c.ORIGINAL_TIME
--      ,c.ORIGINAL_ADQUIRING_INST
--      ,c.ORIGINAL_FORWARDING_INST
--      ,c.REPLA_TRANSACTION_AMOUNT
--      ,c.REPLA_SETTLEMENT_AMOUNT
--      ,c.REPLA_TRANSACTION_FEE
--      ,c.REPLA_SETTLEMENT_FEE
--      ,c.MERCHANT
--      ,c.REQUESTING_INSTITUTION
--      ,c.ACCOUNT_IDENTIFICATION1
--      ,c.ACCOUNT_IDENTIFICATION2
--      ,c.CARDC_ATEGORY
--      ,c.TIERII
--      ,c.CARD_ACCEPTOR_ID
--      ,c.CONCILIA_AUTORIZACION
--      ,c.CONCILIA_LOG_CONTABLE
--      ,c.ROL_TRANSACCION
--      ,c.ID_CLASE_SERVICIO
--      ,c.ID_MEMBRESIA
--      ,c.ID_ORIGEN
      ,c.ID_CODIGO_TRANSACCION
      ,c.ID_CLASE_TRANSACCION
--      ,c.FECHA_PROCESO
      ,c.ID_CLIENTE
--      ,c.ID_EMPRESA
--      ,c.ID_PROCESO
      ,c.ID_CANAL
--      ,c.ID_BIN
--      ,c.CUENTA_FROM
--      ,c.CUENTA_TO
--      ,c.NUMERO_DOCUMENTO
--      ,c.CUENTA_CARGO
--      ,c.CUENTA_ABONO
--      ,c.CODIGO_ANALITICO
--      ,c.CONTAB_FONDOS
--      ,c.ID_ATM
--      ,c.FECHA_CONCILIA_LOG
--      ,c.ID_SUB_BIN
--      ,c.ID_INSTITUCION
--      ,c.PAN_ENTRY_MODE
--      ,c.PIN_ENTRY_CAPABILITY
--      ,c.INSTITUCION_EMISORA
      ,c.INSTITUCION_RECEPTORA
      ,c.CODIGOSEGUIMIENTO
      ,c.TARJETA_PRESENTE
      ,c.DIA AS "Día Compra"
      ,c.PERIODO AS "Periodo Compra"
      ,c.SELLER
      ,c.TIPO AS "Tipo"
      ,c.MONTO AS "Monto compra"
      ,c.CONTADOR AS "Compras contador"
      ,c.RESULTADO_COMPRA AS "Resultado compra"
      ,c.NOMBRE AS "Nombre"
      ,qc.DNI_CELULAR
      ,qc.TIPODOCUMENTO AS "Tipo documento"
      ,qc.DNI
      ,qc.CELULAR AS "Celular"
      ,qc.FIRST_NAME AS "Primer nombre"
      ,qc.SECOND_NAME AS "Segundo nombre"
      ,qc.THIRD_NAME AS "Tercer nombre"
      ,qc.FOURTH_NAME AS "Cuarto nombre"
      ,qc.FIFTH_NAME AS "Quinto nombre"
      ,qc.FIRST_SURNAME AS "Primer apellido"
      ,qc.SECOND_SURNAME AS "Segundo apellido"
      ,qc.EMAIL AS "Correo electrónico"
      ,qc.AFFILIATE_CODE
      ,qc.STATUS AS "Status"
      ,qc.HABILITADO_RECARGA AS "Habilitado recarga"
      ,qc.SEGMENTO AS "Segmento"
      ,qc.FECNACIMIENTO AS "Fecha nacimiento"
      ,qc.EDAD AS "Edad"
      ,qc.FECCREACION AS "Fec. Cre. Cli."
      ,qc.HORACREACION AS "Hora Cre. Cli."
      ,qc.HORAHHCREACION AS "Hora HH Cre. Cli."
      ,qc.AFILIADOR AS "Afiliador"
      ,qc.AFILIADO AS "Afiliado"
      ,qc.GENERO AS "Genero"
      ,qc.PER_CREACION_CLIENTE AS "Periodo Cre. Cli."
      ,qc.SALDOBE AS "B.E. Saldo"
      ,qc.SALDOMC AS "MC Saldo"
FROM ods.COMPRAS c
       LEFT JOIN ods.CLIENTE_T2 qc ON c.DNI_CELULAR = qc.DNI_CELULAR;

--SELECT * from ods.CLIENTE_T4 WHERE CELULAR = '997280449'
--SELECT * from ods.CLIENTE_T4 WHERE CELULAR = '933547710'
--SELECT TOP 100 * FROM ods.Q_COMPRAS WHERE dni_celular = '07249095932519912'

--SELECT * FROM bds.Q_CLIENTE WHERE CELULAR = '997280449';
--SELECT * FROM bds.Q_COMPRAS_CLIENTES WHERE CELULAR = '997280449';
--SELECT * FROM bds.Q_MOVMNTS_CLIENTES WHERE CELULAR = '997280449';

GO